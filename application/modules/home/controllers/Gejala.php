<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gejala extends Admin_Controller {

    protected $module = "home/gejala";

    public function index($id_topik) {
        $this->output->script_foot("$this->module/index.js");

        $gejala = $this->db
            ->select("t.id_topik, nama_topik, id_gejala, nama_gejala, bobot")
            ->from("gejala g")
            ->join("topik t", "t.id_topik = g.id_topik")
            ->where("t.id_topik", $id_topik)
            ->get();
        
        $topik = $this->db
            ->from("topik t")
            ->where("t.id_topik", $id_topik)
            ->get();
        $val = $topik->row();
        $id_topik = $val->id_topik;
        $nama_topik = $val->nama_topik;

        $data = array(
            "id_topik" => $id_topik,
            "nama_topik" => $nama_topik,
            "gejala" => $gejala,
        );
        $this->load->view("$this->module/index", $data);
    }

    public function proses() {
        $this->output->unset_template();

        $id_topik = $this->input->post("id_topik");
        $id_gejala = $this->input->post("id_gejala");
        $nama_gejala = $this->input->post("nama_gejala");
        $bobot = $this->input->post("bobot");
        $keterangan = $this->input->post("keterangan");

        $stat = false;

        if (!empty($id_gejala)) {
            $proses = $this->db
                ->where("id_gejala", $id_gejala)
                ->update("gejala", array(
                    "id_topik" => $id_topik,
                    "nama_gejala" => $nama_gejala,
                    "bobot" => $bobot,
                    "keterangan" => $keterangan,
                ));
        } else {
            $proses = $this->db->insert("gejala", array(
                "id_topik" => $id_topik,
                "nama_gejala" => $nama_gejala,
                "bobot" => $bobot,
                "keterangan" => $keterangan,
            ));
        }

        if ($proses) {
            $stat = true;
        }

        echo json_encode(array(
            "stat" => $stat
        ));
    }

    public function hapus() {
        $this->output->unset_template();

        $id_gejala = $this->input->post("id_gejala");
        $stat = false;

        if (!empty($id_gejala)) {
            $proses = $this->db
                ->where("id_gejala", $id_gejala)
                ->delete("gejala");
        } else {
            show_404();
        }

        if ($proses) {
            $stat = true;

            @$this->db
                ->where("id_gejala = $id_gejala")
                ->delete("relasi_hipotesa_gejala");
        }

        echo json_encode(array(
            "stat" => $stat
        ));
    }

    public function get_data () {
        $this->output->unset_template();

        $stat = false;
        $data = array(
            "id_topik" => "",
            "nama_gejala" => "",
            "bobot" => "",
            "keterangan" => "",
        );

        if (
            $this->input->post()
            AND !empty($this->input->post("id_gejala"))
        ) {
            $id_gejala = $this->input->post("id_gejala");
            $sql = $this->db
                ->where("id_gejala = $id_gejala")
                ->get("gejala");

            if ($sql->num_rows() > 0) {
                $stat = true;
                $val = $sql->row();
                $data = array(
                    "id_topik" => $val->id_topik,
                    "nama_gejala" => $val->nama_gejala,
                    "bobot" => $val->bobot,
                    "keterangan" => empty($val->keterangan) ? "" : $val->keterangan,
                );
            }

        }
        echo json_encode(array(
            "stat" => $stat,
            "data" => $data,
        ));
    }
}

/* End of file gejala.php */
