<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosa extends Admin_Controller {

    protected $module = "home";
    
    public function index($id_topik) {
        $this->output->script_head("$this->module/diagnosa/index.css");
        $this->output->script_foot("$this->module/diagnosa/index.js");

        // load gejala
        $gejala = $this->db
            ->where("id_topik", $id_topik)
            ->get("gejala");
        
        // get nama topik
        $sql = $this->db
            ->where("id_topik", $id_topik)
            ->get("topik");
        $val = $sql->row();
        $nama_topik = $val->nama_topik;

        $data = array(
            "id_topik" => $id_topik,
            "nama_topik" => $nama_topik,
            "gejala" => $gejala,
        );
        $this->load->view("$this->module/diagnosa/index", $data);
    }

    public function proses() {
        header("content-type: application/json");
        $this->output->unset_template();
        $id_topik = $this->input->post('id_topik');

        $sql = $this->db
            ->where("id_topik = $id_topik")
            ->get("hipotesa");
        $hipotesa = array();
        foreach ($sql->result() as $h) {
            $hipotesa[$h->id_hipotesa] = array(
                "h" => $h->nama_hipotesa,
                "solusi" => $h->solusi,
            );
        }
        
        // $hasil = $this->bayes();
        $hasil = $this->ds();
        $bobot_tinggi = 0;
        $diagnosa = array();
        $solusi = array();
        foreach ($hasil as $h) {
            if ($h['bobot'] > $bobot_tinggi) {
                $hipotesa_tinggi = "";
                $bobot_tinggi = ($h['bobot'] * 100);
                foreach ($h['h'] as $val) {
                    if ($val > 0) {
                        $hipotesa_tinggi .= $hipotesa[$val]['h'] . "<br>";
                        array_push($solusi, array(
                            "h" => $hipotesa[$val]['h'],
                            "solusi" => $hipotesa[$val]['solusi']
                        ));
                    } else {
                        $hipotesa_tinggi = "&#x0398";
                    }
                }
                $hipotesa_tinggi = rtrim($hipotesa_tinggi, "<br>");
            }

            $semua_hipotesa = "";
            foreach ($h['h'] as $val) {
                if ($val > 0) {
                    $semua_hipotesa .= $hipotesa[$val]['h'] . "<br>";
                } else {
                    $semua_hipotesa = "&#x0398";
                }
            }
            $semua_hipotesa = rtrim($semua_hipotesa, "<br>");

            array_push($diagnosa, array(
                "hipotesa" => $semua_hipotesa,
                "bobot" => ($h['bobot'] * 100) . " %"
            ));
        }
        
        if ($bobot_tinggi <= 50) {
            $kemungkinan = "Terdapat <b>sedikit kemungkinan atau kemungkinan kecil</b> terdiagnosa <b>$hipotesa_tinggi</b>";
        } elseif ($bobot_tinggi < 80) {
            $kemungkinan = "Terdapat <b>kemungkinan</b> terdiagnosa <b>$hipotesa_tinggi</b>";
        } elseif ($bobot_tinggi < 100) {
            $kemungkinan = "Terdapat <b>kemungkinan besar</b> terdiagnosa <b>$hipotesa_tinggi</b>";
        } else {
            $kemungkinan = "<b>Sangat yakin</b> bahwa terdiagnosa <b>$hipotesa_tinggi</b>";
        }

        echo json_encode(array(
            'kemungkinan' => $kemungkinan . " Dengan bobot kepercayaan sebesar <b>$bobot_tinggi %</b>",
            'diagnosa' => $diagnosa,
            'solusi' => $solusi,
        ));
    } 

    protected function ds () {
        $id_gejala = $this->input->post('id_gejala');
        $id_topik = $this->input->post('id_topik');

        $list_id_gejala = "";
        foreach ($id_gejala as $val) {
            $list_id_gejala .= "$val,";
        }
        $list_id_gejala = rtrim($list_id_gejala, ',');

        $sql = $this->db
            ->select("r.id_hipotesa, r.id_gejala, bobot")
            ->where("h.id_topik = $id_topik")
            ->where("r.id_gejala IN($list_id_gejala)")
            ->from("hipotesa h")
            ->join("relasi_hipotesa_gejala r", "h.id_hipotesa = r.id_hipotesa")
            ->join("gejala g", "g.id_gejala = r.id_gejala")
            ->order_by("h.id_hipotesa")
            ->get();
        
        $persamaan_1 = array();
        foreach ($sql->result() as $data) {
            if (empty($persamaan_1[$data->id_gejala])) {
                $persamaan_1[$data->id_gejala] = array();
            }
            if (empty($persamaan_1[$data->id_gejala]['h'])) {
                $persamaan_1[$data->id_gejala]['h'] = array();
            }
            array_push($persamaan_1[$data->id_gejala]['h'], $data->id_hipotesa);
            $persamaan_1[$data->id_gejala] += array(
                "bobot" => $data->bobot
            );
        }

        // echo json_encode($persamaan_1); die;
        
        $a = 0;
        $x = $y = array();
        foreach ($persamaan_1 as $data) {
            if ($a == 0) {
                array_push($x, $data);
                array_push($x, array(
                    "h" => array(
                        0
                    ),
                    "bobot" => (1 - $data['bobot'])
                ));
            } else {
                array_push($y, $data);
                array_push($y, array(
                    "h" => array(
                        0
                    ),
                    "bobot" => (1 - $data['bobot'])
                ));

                $b = 0;
                $tmp_bobot = 0;
                $new_x = array();
                $count_x = count($x);
                $teta_x = 0;
                $perulangan_x = 0;

                for ($i=0; $i < 2; $i++) {
                    for ($j=0; $j < $count_x; $j++) { 
                        $perulangan_x++;
                        $tmp_bobot = $x[$j]['bobot'] * $y[$i]['bobot'];

                        if ($i == 0) {
                            $xx = "";
                            $bersinggungan = false;
                            foreach ($x[$j]['h'] as $k) {
                                if (in_array($k, $y[$i]['h'])) {
                                    $xx .= "$k,";
                                    $bersinggungan = true;
                                }
                            }
                            
                            if ($bersinggungan) {
                                $xx = rtrim($xx, ',');
                                $xx = explode(',', $xx);
                                sort($xx);
                            } elseif (!in_array(0, $x[$j]['h'])) {
                                $xx = array("teta_x");
                                $teta_x += $tmp_bobot;
                                continue;
                            } else {
                                $xx = $y[$i]['h'];
                            }
                        } else {
                            $xx = $x[$j]['h'];
                        }

                        $inject = true;
                        if ($perulangan_x > 0) {
                            foreach ($new_x as $key => $cek_new) {
                                if (
                                    count(array_diff($cek_new['h'], $xx)) == 0
                                    AND count($cek_new['h']) == count($xx)
                                ) {
                                    $new_x[$key] = array(
                                        "h" => $cek_new['h'],
                                        "bobot" => $cek_new['bobot'] + $tmp_bobot
                                    );
                                    $inject = false;
                                }
                            }
                        }

                        if ($inject) {
                            array_push($new_x, array(
                                "h" => $xx,
                                "bobot" => $tmp_bobot
                            ));
                        }
                        // echo json_encode($new_x); die;
                    }
                }

                if ($teta_x > 0) {
                    foreach ($new_x as $key => $final_cek) {
                        $tmp_bobot = $final_cek['bobot'] / (1-$teta_x);
                        $new_x[$key] = array(
                            "h" => $final_cek['h'],
                            "bobot" => $tmp_bobot
                        );
                    }
                }
                
                $x = $new_x;
                $y = array();
            }
            $a++;
        }
        return $x;
    }

    protected function bayes() {
        $g = $this->input->post("gejala");
        $total_bobot = 0;
        foreach ($g as $data) {
            $total_bobot += floatval($data);
        }
        $total_bobot = number_format($total_bobot, 5);
        
        $PHi = 0;
        foreach ($g as $key => $data) {
            $PHi += ($data / $total_bobot) * $data;
            $PH[$key] = ($data / $total_bobot);
        }
        $PHi = number_format($PHi, 5);
        
        $PHiE = $bayes = 0;
        foreach ($g as $key => $data) {
            $PHiE = ($data * $PH[$key]) / $PHi;
            $bayes += $data * $PHiE;
        }
        return number_format($bayes * 100, 2);
    }
}

/* End of file Diagnosa.php */