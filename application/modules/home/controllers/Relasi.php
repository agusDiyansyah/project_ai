<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Relasi extends Admin_Controller {

    protected $module = "home/relasi";

    public function index($id_topik) {
        $this->output->script_foot("$this->module/index.js");

        $sql = $this->db
            ->select("nama_gejala, h.id_hipotesa")
            ->from("topik t")
            ->join("hipotesa h", "t.id_topik = h.id_topik")
            ->join("relasi_hipotesa_gejala r", "h.id_hipotesa = r.id_hipotesa")
            ->join("gejala g", "g.id_gejala = r.id_gejala")
            ->where("t.id_topik = $id_topik")
            ->get();

        $relasi = array();
        foreach ($sql->result() as $val) {
            if (empty($relasi[$val->id_hipotesa])) {
                $relasi[$val->id_hipotesa] = array();
            }
            array_push($relasi[$val->id_hipotesa], $val->nama_gejala);
        }

        $hipotesa = $this->db
            ->from("hipotesa h")
            ->where("id_topik", $id_topik)
            ->get();
        
        // get nama topik
        $sql = $this->db
            ->where("id_topik", $id_topik)
            ->get("topik");
        $val = $sql->row();
        $nama_topik = $val->nama_topik;

        $data = array(
            "id_topik" => $id_topik,
            "nama_topik" => $nama_topik,
            "relasi" => $relasi,
            "hipotesa" => $hipotesa,
        );
        $this->load->view("$this->module/index", $data);
    }

    public function proses() {
        $this->output->unset_template();

        $id_hipotesa = $this->input->post("id_hipotesa");
        $id_gejala = $this->input->post("id_gejala");

        $this->db
            ->where("id_hipotesa = $id_hipotesa")
            ->delete("relasi_hipotesa_gejala");

        foreach ($id_gejala as $val) {
            $proses = $this->db->insert("relasi_hipotesa_gejala", array(
                "id_hipotesa" => $id_hipotesa,
                "id_gejala" => $val,
            ));
        }
    }

    public function hapus() {
        $this->output->unset_template();

        $id_hipotesa = $this->input->post("id_hipotesa");
        $stat = false;

        if (!empty($id_hipotesa)) {
            $proses = $this->db
                ->where("id_hipotesa", $id_hipotesa)
                ->delete("hipotesa");
        } else {
            show_404();
        }

        if ($proses) {
            $stat = true;
        }

        echo json_encode(array(
            "stat" => $stat
        ));
    }

    public function get_data () {
        $this->output->unset_template();

        if (
            $this->input->post()
            AND !empty($this->input->post("id_topik"))
            AND !empty($this->input->post("id_hipotesa"))
        ) {
            $id_topik = $this->input->post("id_topik");
            $id_hipotesa = $this->input->post("id_hipotesa");
            $stat = false;
            $data = array();
            $relasi = array();
            
            $sql = $this->db
                ->where("id_hipotesa = $id_hipotesa")
                ->get("relasi_hipotesa_gejala");
            foreach ($sql->result() as $val) {
                array_push($relasi, $val->id_gejala);
            }

            $gejala = $this->db
                ->where("id_topik = $id_topik")
                ->get("gejala");

            if ($gejala->num_rows() > 0) {
                $stat = true;
                foreach ($gejala->result() as $val) {
                    array_push($data, array(
                        "id_gejala" => $val->id_gejala,
                        "nama_gejala" => $val->nama_gejala,
                        "active" => ((in_array($val->id_gejala, $relasi)) ? 1 : 0),
                    ));
                }
            }

            echo json_encode(array(
                "stat" => $stat,
                "data" => $data,
            ));
        } else {
            show_404();
        }
    }
}

/* End of file hipotesa.php */
