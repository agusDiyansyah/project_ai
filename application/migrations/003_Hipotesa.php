<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Hipotesa extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS hipotesa;
		");
		$this->db->query("
			CREATE TABLE `hipotesa` (
				`id_hipotesa` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`id_topik` int(11),
				`nama_hipotesa` varchar(255),
				`solusi` text,
				PRIMARY KEY (`id_hipotesa`)
			);
		");
	}

	public function down () {}
	
}