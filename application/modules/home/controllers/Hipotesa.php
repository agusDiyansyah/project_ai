<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hipotesa extends Admin_Controller {

    protected $module = "home";

    public function index($id_topik) {
        $this->output->script_foot("$this->module/hipotesa/index.js");

        $hipotesa = $this->db
            ->from("hipotesa h")
            ->where("id_topik", $id_topik)
            ->get();
        
        // get nama topik
        $sql = $this->db
            ->where("id_topik", $id_topik)
            ->get("topik");
        $val = $sql->row();
        $nama_topik = $val->nama_topik;

        $data = array(
            "id_topik" => $id_topik,
            "nama_topik" => $nama_topik,
            "hipotesa" => $hipotesa,
        );
        $this->load->view("$this->module/hipotesa/index", $data);
    }

    public function proses() {
        $this->output->unset_template();

        $id_topik = $this->input->post("id_topik");
        $id_hipotesa = $this->input->post("id_hipotesa");
        $nama_hipotesa = $this->input->post("nama_hipotesa");
        $solusi = $this->input->post("solusi");
        $stat = false;

        if (!empty($id_hipotesa)) {
            $proses = $this->db
                ->where("id_hipotesa", $id_hipotesa)
                ->update("hipotesa", array(
                    "id_topik" => $id_topik,
                    "nama_hipotesa" => $nama_hipotesa,
                    "solusi" => $solusi,
                ));
        } else {
            $proses = $this->db->insert("hipotesa", array(
                "id_topik" => $id_topik,
                "nama_hipotesa" => $nama_hipotesa,
                "solusi" => $solusi,
            ));
        }

        if ($proses) {
            $stat = true;
        }

        echo json_encode(array(
            "stat" => $stat
        ));
    }

    public function hapus() {
        $this->output->unset_template();

        $id_hipotesa = $this->input->post("id_hipotesa");
        $stat = false;

        if (!empty($id_hipotesa)) {
            $proses = $this->db
                ->where("id_hipotesa", $id_hipotesa)
                ->delete("hipotesa");
        } else {
            show_404();
        }

        if ($proses) {
            $stat = true;

            @$this->db
                ->where("id_hipotesa = $id_hipotesa")
                ->delete("relasi_hipotesa_gejala");
        }

        echo json_encode(array(
            "stat" => $stat
        ));
    }

    public function get_data () {
        $this->output->unset_template();

        $stat = false;
        $data = array(
            "id_topik" => "",
            "nama_hipotesa" => "",
            "solusi" => "",
        );

        if (
            $this->input->post()
            AND !empty($this->input->post("id_hipotesa"))
        ) {
            $id_hipotesa = $this->input->post("id_hipotesa");
            $sql = $this->db
                ->where("id_hipotesa = $id_hipotesa")
                ->get("hipotesa");

            if ($sql->num_rows() > 0) {
                $stat = true;
                $val = $sql->row();
                $data = array(
                    "id_topik" => $val->id_topik,
                    "nama_hipotesa" => $val->nama_hipotesa,
                    "solusi" => empty($val->solusi) ? "" : $val->solusi,
                );
            }

        }
        echo json_encode(array(
            "stat" => $stat,
            "data" => $data,
        ));
    }
}

/* End of file hipotesa.php */
