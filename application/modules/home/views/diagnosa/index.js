$(document).ready(function () {
    $('.aksi-relasi').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('bg-active');
        var cb = $(this).find('.id_gejala');
        cb.prop("checked", !cb.prop("checked"));
    });

    $('.btn-diagnosa').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "post",
            url: "<?= base_url('home/diagnosa/proses') ?>",
            data: $('.form-diagnosa').serialize(),
            dataType: "json",
            success: function (json) {
                $('.kemungkinan').html(json.kemungkinan);
                $('.solusi').find('table tbody').html('');
                $('.lainnya').find('tbody').html('');
                $.each(json.solusi, function (i, v) { 
                    var tb = '';
                    tb += '<tr>';
                    tb += '    <td>'+ v.h +'</td>';
                    tb += '    <td>'+ v.solusi +'</td>';
                    tb += '</tr>';

                    $('.solusi').find('table tbody').append(tb);
                });

                $.each(json.diagnosa, function (i, v) { 
                    tb = '';
                    tb += '<tr>';
                    tb += '    <td>'+ v.hipotesa +'</td>';
                    tb += '    <td>'+ v.bobot +'</td>';
                    tb += '</tr>';

                    $('.lainnya').find('tbody').append(tb);
                });
            }
        });
    });
});