$(document).ready(function () {
    $('.btn-tambah').on('click', function (e) {
        e.preventDefault();
        form($(this));
    });

    $('.btn-edit').on('click', function (e) {
        e.preventDefault();
        form($(this));
    });

    $('.btn-hapus').on('click', function (e) {
        e.preventDefault();
        var m = $('.modal');
        var id_topik = ($(this).data('id_topik')) ? $(this).data('id_topik') : "";

        m.find('.modal-title').html('HAPUS TOPIK');
        m.find('.modal-body').html("ANDA YAKIN AKAN MENGHAPUS DATA INI");
        m.modal();

        m.find('.btn-proses').off().on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url("home/hapus") ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    id_topik: id_topik,
                },
                success: function (json) {
                    if (json.stat) {
                        location.reload();
                    }
                }
            });
        });
    });
});

function form (obj) {
    var m = $('.modal');
    var form = '';
    var nama_topik = (obj.data('nama_topik')) ? obj.data('nama_topik') : "";
    var id_topik = (obj.data('id_topik')) ? obj.data('id_topik') : "";
    var tipe_aksi = (obj.data('id_topik') != '') ? "UBAH" : "TAMBAH";

    form += '<form class="form-topik">';
    form += '   <div class="row">';
    form += '       <div class="col-md-12">';
    form += '           <div class="form-group">';
    form += '               <label for="">NAMA TOPIK</label>';
    form += '               <input type="text" value="' + nama_topik + '" name="nama_topik" class="nama_topik form-control" required>';
    form += '               <input type="text" value="'+ id_topik +'" name="id_topik" class="id_topik form-control hide">';
    form += '           </div>';
    form += '       </div>';
    form += '   </div>';
    form += '   <button type="submit" class="hidden"></button>';
    form += '</form>';

    m.find('.modal-title').html(tipe_aksi + ' TOPIK');
    m.find('.modal-body').html(form);
    m.modal();

    m.find('.btn-proses').off().on('click', function (e){
        e.preventDefault();
        m.find('.form-topik').submit();
    });
    
    m.find('.form-topik').validate({
        ignore: [],
        rules: {
            nama_topik: {required: true},
        },
        messages: {
            nama_topik: {required: "Nama topik tidak boleh kosong"},
        },
        submitHandler: function () {
            $.ajax({
                url: '<?php echo base_url("home/proses") ?>',
                type: 'post',
                dataType: 'json',
                data: m.find('.form-topik').serialize(),
                success: function (json) {
                    if (json.stat) {
                        location.reload();
                    }
                }
            });
        }
    });
}