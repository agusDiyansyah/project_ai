$(document).ready(function () {
    $('.btn-tambah').on('click', function (e) {
        e.preventDefault();
        form($(this));
    });

    $('.btn-edit').on('click', function (e) {
        e.preventDefault();
        form($(this));
    });

    $('.btn-hapus').on('click', function (e) {
        e.preventDefault();
        var m = $('.modal');
        var id_gejala = ($(this).data('id_gejala')) ? $(this).data('id_gejala') : "";

        m.find('.modal-title').html('HAPUS TOPIK');
        m.find('.modal-body').html("ANDA YAKIN AKAN MENGHAPUS DATA INI");
        m.modal();

        m.find('.btn-proses').off().on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url("home/gejala/hapus") ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    id_gejala: id_gejala,
                },
                success: function (json) {
                    if (json.stat) {
                        location.reload();
                    }
                }
            });
        });
    });
});

function form (obj) {
    var id_gejala = (obj.data('id_gejala')) ? obj.data('id_gejala') : "";
    var id_topik = obj.data("id_topik");

    $.ajax({
        url: '<?php echo base_url("home/gejala/get_data") ?>',
        type: 'post',
        dataType: 'json',
        data: {
            id_gejala: id_gejala,
        },
        success: function (json) {
            var m = $('.modal');
            var form = '';
            var aksi_name = (id_gejala != '') ? 'UBAH' : 'TAMBAH';

            form += '<form class="form-gejala">';
            form += '   <div class="row">';
            form += '       <div class="col-md-12">';
            form += '           <div class="form-group">';
            form += '               <label for="">GEJALA</label>';
            form += '               <input required type="text" value="' + json.data.nama_gejala + '" name="nama_gejala" class="nama_gejala form-control">';
            form += '           </div>';
            form += '       </div>';
            form += '       <div class="col-md-12">';
            form += '           <div class="form-group">';
            form += '               <label for="">BOBOT</label>';
            form += '               <input required type="text" value="' + json.data.bobot + '" name="bobot" class="bobot form-control">';
            form += '           </div>';
            form += '       </div>';
            form += '       <div class="col-md-12">';
            form += '           <div class="form-group">';
            form += '               <label for="">CIRI-CIRI</label>';
            form += '               <textarea class="keterangan form-control" name="keterangan">' + json.data.keterangan + '</textarea>';
            form += '           </div>';
            form += '       </div>';
            form += '   </div>';
            form += '   <button class="hidden" type="submit"></button>';
            form += '</form>';

            m.find('.modal-title').html(aksi_name + ' GEJALA');
            m.find('.modal-body').html(form);
            m.modal();

            m.find('.btn-proses').on('click', function () {
                m.find('.form-gejala').submit();
            });

            m.find('.form-gejala').validate({
                ignore: [],
                rules: {
                    nama_gejala: {required: true},
                    bobot: {required: true},
                },
                messages: {
                    nama_gejala: {required: "Nama gejala tidak boleh kosong"},
                    bobot: {required: "Bobot tidak boleh kosong"},
                },
                submitHandler: function () {
                    $.ajax({
                        url: '<?php echo base_url("home/gejala/proses") ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            id_gejala: id_gejala,
                            id_topik: id_topik,
                            nama_gejala: m.find('.nama_gejala').val(),
                            bobot: m.find('.bobot').val(),
                            keterangan: m.find('.keterangan').val(),
                        },
                        success: function (json) {
                            if (json.stat) {
                                location.reload();
                            }
                        }
                    });
                }
            });
        }
    });
}