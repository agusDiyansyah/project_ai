$(document).ready(function () {
    $('.btn-relasi').on('click', function (e) {
        e.preventDefault();
        form($(this));
    });

    $('.btn-hapus').on('click', function (e) {
        e.preventDefault();
        var m = $('.modal');
        var id_relasi = ($(this).data('id_relasi')) ? $(this).data('id_relasi') : "";

        m.find('.modal-title').html('HAPUS RELASI');
        m.find('.modal-body').html("ANDA YAKIN AKAN MENGHAPUS DATA INI");
        m.modal();

        m.find('.btn-proses').off().on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url("home/relasi/hapus") ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    id_relasi: id_relasi,
                },
                success: function (json) {
                    if (json.stat) {
                        location.reload();
                    }
                }
            });
        });
    });
});

function form (obj) {
    var id_topik = (obj.data('id_topik')) ? obj.data('id_topik') : "";
    var id_hipotesa = (obj.data('id_hipotesa')) ? obj.data('id_hipotesa') : "";
    var nama_hipotesa = (obj.data('nama_hipotesa')) ? obj.data('nama_hipotesa') : "";

    $.ajax({
        url: '<?php echo base_url("home/relasi/get_data") ?>',
        type: 'post',
        dataType: 'json',
        data: {
            id_topik: id_topik,
            id_hipotesa: id_hipotesa,
        },
        success: function (json) {
            var m = $('.modal');
            var form = '';
            var no = 0;
            var active = checked = "";
            
            form += '<form class="form-relasi">';
            form += '   <input name="id_hipotesa" value="'+ id_hipotesa +'" class="hidden">';
            form += '   <div class="row">';
            form += '       <div class="col-md-12">';
            form += '           <table class="table table-hover table-striped">';
            form += '               <thead>';
            form += '                   <tr>';
            form += '                       <th width="2%">NO</th>';
            form += '                       <th>GEJALA</th>';
            form += '                   </tr>';
            form += '               </thead>';
            form += '               <tbody>';
            $.each(json.data, function (index, val) { 
                no++;
                active = (val.active == 1) ? 'bg-active' : '';
                checked = (val.active == 1) ? 'checked' : '';

                form += '               <tr style="cursor: pointer" class="aksi-relasi '+ active +'">';
                form += '                   <td>'+ no +'</td>';
                form += '                   <td><input '+ checked +' type="checkbox" value="'+ val.id_gejala +'" name="id_gejala[]" class="id_gejala hidden">'+ val.nama_gejala +'</td>';
                form += '               </tr>';
            });
            form += '               </tbody>';
            form += '           </table>';
            form += '       </div>';
            form += '   <button class="hidden" type="submit"></button>';
            form += '</form>';
    
            m.find('.modal-title').html(nama_hipotesa);
            m.find('.modal-body').html(form);
            m.modal();

            m.find('.aksi-relasi').on('click', function (e) {
                e.preventDefault();
                $(this).toggleClass('bg-active');
                var cb = $(this).find('.id_gejala');
                cb.prop("checked", !cb.prop("checked"));
            });

            m.find('.btn-proses').off().on('click', function (e) {
                e.preventDefault();
                $.ajax({
                    url: '<?php echo base_url("home/relasi/proses") ?>',
                    type: 'post',
                    data: $('.form-relasi').serialize(),
                    success: function () {
                        location.reload();
                    }
                });
            });
        }
    });
}