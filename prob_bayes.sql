/*
 Navicat Premium Data Transfer

 Source Server         : LOCALHOST
 Source Server Type    : MySQL
 Source Server Version : 100203
 Source Host           : localhost:3306
 Source Schema         : prob_bayes

 Target Server Type    : MySQL
 Target Server Version : 100203
 File Encoding         : 65001

 Date: 24/01/2019 11:49:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gejala
-- ----------------------------
DROP TABLE IF EXISTS `gejala`;
CREATE TABLE `gejala`  (
  `id_gejala` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_topik` int(11) NULL DEFAULT NULL,
  `nama_gejala` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bobot` decimal(10, 2) NULL DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id_gejala`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gejala
-- ----------------------------
INSERT INTO `gejala` VALUES (1, 3, 'Badan Panas', 0.40, NULL, '');
INSERT INTO `gejala` VALUES (2, 3, 'Sakit Kepala', 0.50, NULL, '');
INSERT INTO `gejala` VALUES (3, 3, 'Bersin-bersin', 0.60, NULL, '');
INSERT INTO `gejala` VALUES (4, 3, 'Batuk', 0.40, NULL, '');
INSERT INTO `gejala` VALUES (5, 3, 'Pilek, Hidung Buntu', 0.80, NULL, '');
INSERT INTO `gejala` VALUES (6, 3, 'Badan Lemas', 0.30, NULL, '');
INSERT INTO `gejala` VALUES (7, 3, 'Kedinginan', 0.30, NULL, '');
INSERT INTO `gejala` VALUES (8, 1, 'Di stater elektrik sulit', 0.80, NULL, '');
INSERT INTO `gejala` VALUES (9, 1, 'Klakson tidak bunyi', 0.90, NULL, '');
INSERT INTO `gejala` VALUES (10, 1, 'Reating tidak bekerja', 0.90, NULL, '');
INSERT INTO `gejala` VALUES (11, 1, 'Di stater manual sulit', 0.70, NULL, '');
INSERT INTO `gejala` VALUES (12, 1, 'Suara knalpot sering meletus-meletus', 0.90, NULL, '');
INSERT INTO `gejala` VALUES (13, 1, 'Tarikan berat', 0.70, NULL, '');
INSERT INTO `gejala` VALUES (14, 1, 'Bahan bakar Boros', 0.90, NULL, '');
INSERT INTO `gejala` VALUES (15, 1, 'Keluar asap kehitaman pada knalpot', 0.90, NULL, '');
INSERT INTO `gejala` VALUES (16, 1, 'Bunyi gemlitik pada mesin', 0.80, NULL, '');
INSERT INTO `gejala` VALUES (17, 1, 'Suara mesin kasar', 0.90, NULL, '');
INSERT INTO `gejala` VALUES (18, 1, 'Bunyi kasar saat jalan pelan', 0.90, NULL, '');
INSERT INTO `gejala` VALUES (19, 1, 'Kampas kopling lambat', 0.90, NULL, '');
INSERT INTO `gejala` VALUES (20, 1, 'Lari mbrebet-mbrebet', 0.90, NULL, '');
INSERT INTO `gejala` VALUES (21, 1, 'Motor mati (tidak bisa hidup sama sekali)', 0.90, NULL, '');
INSERT INTO `gejala` VALUES (22, 4, 'Nafsu Makan Berkurang', 0.20, NULL, NULL);
INSERT INTO `gejala` VALUES (23, 4, 'Wajah Terlihat Lesu', 0.20, NULL, NULL);
INSERT INTO `gejala` VALUES (24, 4, 'Berat Badan Menurun', 0.50, NULL, NULL);
INSERT INTO `gejala` VALUES (25, 4, 'Pembengkakan Pada Mulut', 0.50, NULL, NULL);
INSERT INTO `gejala` VALUES (26, 4, 'Pembengkakan Pada Gusi', 0.80, NULL, NULL);
INSERT INTO `gejala` VALUES (27, 4, 'Perubahan Warna Pada Mulut', 0.70, NULL, NULL);
INSERT INTO `gejala` VALUES (28, 4, 'Badan Terlihat Membengkak', 0.40, NULL, NULL);
INSERT INTO `gejala` VALUES (29, 4, 'BAB Jarang', 0.50, NULL, NULL);
INSERT INTO `gejala` VALUES (30, 4, 'BAB Encer', 0.10, NULL, NULL);
INSERT INTO `gejala` VALUES (31, 4, 'Muntah-muntah', 0.50, NULL, NULL);
INSERT INTO `gejala` VALUES (32, 4, 'Terdapat Kutu/Caplak di Badan', 0.90, NULL, NULL);
INSERT INTO `gejala` VALUES (33, 4, 'Terdapat Cairan Pada Hidung', 0.80, NULL, NULL);
INSERT INTO `gejala` VALUES (34, 4, 'Bernafas Dengan Mulut Terbuka', 0.70, NULL, NULL);
INSERT INTO `gejala` VALUES (35, 4, 'Keluar Cairan Dari Hidung/Mulut', 0.90, NULL, NULL);
INSERT INTO `gejala` VALUES (36, 4, 'Nafas Yang Berbunyi(Mendengkur)', 0.80, NULL, NULL);
INSERT INTO `gejala` VALUES (37, 4, 'Selalu Melihat Keatas(Stargazing)', 0.90, NULL, NULL);
INSERT INTO `gejala` VALUES (38, 4, 'Tidak Dapat Berdiri', 0.60, NULL, NULL);
INSERT INTO `gejala` VALUES (39, 4, 'Selalu Tergolek Kearah Belakang', 0.80, NULL, NULL);
INSERT INTO `gejala` VALUES (40, 4, 'Tidak Merespon Gerakan', 0.20, NULL, NULL);
INSERT INTO `gejala` VALUES (41, 4, 'Besar Mata Pupil Tidak Seimbang', 0.50, NULL, NULL);
INSERT INTO `gejala` VALUES (42, 4, 'Mengalami Kelumpuhan', 0.60, NULL, NULL);
INSERT INTO `gejala` VALUES (43, 4, 'Lendir Kental Dihidung', 0.90, NULL, NULL);
INSERT INTO `gejala` VALUES (44, 4, 'Bengkak Dibawah Mulut', 0.60, NULL, NULL);
INSERT INTO `gejala` VALUES (45, 4, 'Ada Massa Padat Di Kloaka(Anus)', 0.90, NULL, NULL);
INSERT INTO `gejala` VALUES (46, 4, 'Rahang Bawah Bernanah', 0.70, NULL, NULL);
INSERT INTO `gejala` VALUES (47, 4, 'Ada Plak Kemerahan Di Gusi', 0.90, NULL, NULL);
INSERT INTO `gejala` VALUES (48, 4, 'Ada Benjol Dirahang Atas', 0.40, NULL, NULL);
INSERT INTO `gejala` VALUES (49, 4, 'Jalannya Berputar-putar', 0.70, NULL, NULL);
INSERT INTO `gejala` VALUES (50, 4, 'Ada Jamur Dimulut', 0.20, NULL, NULL);
INSERT INTO `gejala` VALUES (51, 4, 'Kepala Terpelinting', 0.40, NULL, NULL);
INSERT INTO `gejala` VALUES (82, 6, 'Lemas/Lesu', 0.20, NULL, '');
INSERT INTO `gejala` VALUES (83, 6, 'Mual', 1.00, NULL, '');
INSERT INTO `gejala` VALUES (84, 6, 'Muntah', 1.00, NULL, '');
INSERT INTO `gejala` VALUES (85, 6, 'Kram', 1.00, NULL, '');
INSERT INTO `gejala` VALUES (86, 6, 'Sakit Kepala', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (87, 6, 'Panas', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (88, 6, 'Mulut Kering', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (89, 6, 'Perut Kembung', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (90, 6, 'Ruam', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (91, 6, 'Feses Encer', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (92, 6, 'BAB >= 10 x sehari', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (93, 6, 'Suhu Tubuh Naik Turun', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (94, 6, 'Merancau', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (95, 6, 'Tekanan Darah Rendah', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (96, 6, 'Mulas', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (97, 6, 'Lidah kotor', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (98, 6, 'Nadi lemah', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (99, 6, 'Sembelit', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (100, 6, 'BAB 3-9x sehari', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (101, 6, 'Feses Berdarah', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (102, 6, 'Titer Widal Antigen H <=1 / 160', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (103, 6, 'Titer Widal Antigen H <=1/160', 1.00, NULL, NULL);
INSERT INTO `gejala` VALUES (104, 6, 'Titer Widal Antigen A H <= 1/160', 1.00, NULL, NULL);

-- ----------------------------
-- Table structure for hipotesa
-- ----------------------------
DROP TABLE IF EXISTS `hipotesa`;
CREATE TABLE `hipotesa`  (
  `id_hipotesa` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_topik` int(11) NULL DEFAULT NULL,
  `nama_hipotesa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `solusi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id_hipotesa`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hipotesa
-- ----------------------------
INSERT INTO `hipotesa` VALUES (1, 3, 'Anemia', '');
INSERT INTO `hipotesa` VALUES (2, 3, 'Bronkitis', '');
INSERT INTO `hipotesa` VALUES (3, 3, 'Demam', '');
INSERT INTO `hipotesa` VALUES (4, 3, 'Flu', '');
INSERT INTO `hipotesa` VALUES (5, 1, 'ACU', '');
INSERT INTO `hipotesa` VALUES (6, 1, 'Busi', '');
INSERT INTO `hipotesa` VALUES (7, 1, 'Injektor', '');
INSERT INTO `hipotesa` VALUES (8, 1, 'Roller', '');
INSERT INTO `hipotesa` VALUES (9, 1, 'CVT', '');
INSERT INTO `hipotesa` VALUES (10, 1, 'CDI', '');
INSERT INTO `hipotesa` VALUES (11, 4, 'Sariawan (Stomatitis)', 'solusi untuk sariawan');
INSERT INTO `hipotesa` VALUES (12, 4, 'IBD (Inclusion Body Diseae)', 'solusi untuk IBD (Inclusion Body Diseae)');
INSERT INTO `hipotesa` VALUES (13, 4, 'Sembelit (Constipation)', 'solusi untuk Sembelit (Constipation)');
INSERT INTO `hipotesa` VALUES (14, 4, 'Penyakit Kulit Parasit', 'solusi untuk Penyakit Kulit Parasit');
INSERT INTO `hipotesa` VALUES (15, 4, 'Flu (Influenza)', 'solusi untuk Flu (Influenza)');
INSERT INTO `hipotesa` VALUES (26, 6, 'Tifus', NULL);
INSERT INTO `hipotesa` VALUES (27, 6, 'Diare', NULL);
INSERT INTO `hipotesa` VALUES (28, 6, 'Disentri', NULL);
INSERT INTO `hipotesa` VALUES (29, 6, 'Kolera', NULL);
INSERT INTO `hipotesa` VALUES (30, 6, 'DBD', NULL);
INSERT INTO `hipotesa` VALUES (31, 6, 'SF', NULL);
INSERT INTO `hipotesa` VALUES (32, 6, 'Demam', NULL);
INSERT INTO `hipotesa` VALUES (33, 6, 'DR', NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `version` bigint(20) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (4);

-- ----------------------------
-- Table structure for relasi_hipotesa_gejala
-- ----------------------------
DROP TABLE IF EXISTS `relasi_hipotesa_gejala`;
CREATE TABLE `relasi_hipotesa_gejala`  (
  `id_hipotesa` int(11) NULL DEFAULT NULL,
  `id_gejala` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of relasi_hipotesa_gejala
-- ----------------------------
INSERT INTO `relasi_hipotesa_gejala` VALUES (2, 1);
INSERT INTO `relasi_hipotesa_gejala` VALUES (2, 3);
INSERT INTO `relasi_hipotesa_gejala` VALUES (2, 4);
INSERT INTO `relasi_hipotesa_gejala` VALUES (3, 1);
INSERT INTO `relasi_hipotesa_gejala` VALUES (3, 6);
INSERT INTO `relasi_hipotesa_gejala` VALUES (3, 7);
INSERT INTO `relasi_hipotesa_gejala` VALUES (4, 1);
INSERT INTO `relasi_hipotesa_gejala` VALUES (4, 2);
INSERT INTO `relasi_hipotesa_gejala` VALUES (4, 3);
INSERT INTO `relasi_hipotesa_gejala` VALUES (4, 5);
INSERT INTO `relasi_hipotesa_gejala` VALUES (4, 6);
INSERT INTO `relasi_hipotesa_gejala` VALUES (4, 7);
INSERT INTO `relasi_hipotesa_gejala` VALUES (5, 8);
INSERT INTO `relasi_hipotesa_gejala` VALUES (5, 9);
INSERT INTO `relasi_hipotesa_gejala` VALUES (5, 10);
INSERT INTO `relasi_hipotesa_gejala` VALUES (6, 11);
INSERT INTO `relasi_hipotesa_gejala` VALUES (6, 12);
INSERT INTO `relasi_hipotesa_gejala` VALUES (6, 13);
INSERT INTO `relasi_hipotesa_gejala` VALUES (6, 15);
INSERT INTO `relasi_hipotesa_gejala` VALUES (7, 8);
INSERT INTO `relasi_hipotesa_gejala` VALUES (7, 11);
INSERT INTO `relasi_hipotesa_gejala` VALUES (7, 13);
INSERT INTO `relasi_hipotesa_gejala` VALUES (7, 14);
INSERT INTO `relasi_hipotesa_gejala` VALUES (8, 11);
INSERT INTO `relasi_hipotesa_gejala` VALUES (8, 13);
INSERT INTO `relasi_hipotesa_gejala` VALUES (8, 16);
INSERT INTO `relasi_hipotesa_gejala` VALUES (8, 17);
INSERT INTO `relasi_hipotesa_gejala` VALUES (9, 16);
INSERT INTO `relasi_hipotesa_gejala` VALUES (9, 18);
INSERT INTO `relasi_hipotesa_gejala` VALUES (9, 19);
INSERT INTO `relasi_hipotesa_gejala` VALUES (10, 20);
INSERT INTO `relasi_hipotesa_gejala` VALUES (10, 21);
INSERT INTO `relasi_hipotesa_gejala` VALUES (13, 22);
INSERT INTO `relasi_hipotesa_gejala` VALUES (13, 23);
INSERT INTO `relasi_hipotesa_gejala` VALUES (13, 28);
INSERT INTO `relasi_hipotesa_gejala` VALUES (13, 29);
INSERT INTO `relasi_hipotesa_gejala` VALUES (13, 30);
INSERT INTO `relasi_hipotesa_gejala` VALUES (14, 22);
INSERT INTO `relasi_hipotesa_gejala` VALUES (14, 23);
INSERT INTO `relasi_hipotesa_gejala` VALUES (14, 24);
INSERT INTO `relasi_hipotesa_gejala` VALUES (14, 31);
INSERT INTO `relasi_hipotesa_gejala` VALUES (14, 32);
INSERT INTO `relasi_hipotesa_gejala` VALUES (11, 22);
INSERT INTO `relasi_hipotesa_gejala` VALUES (11, 24);
INSERT INTO `relasi_hipotesa_gejala` VALUES (11, 25);
INSERT INTO `relasi_hipotesa_gejala` VALUES (11, 26);
INSERT INTO `relasi_hipotesa_gejala` VALUES (11, 27);
INSERT INTO `relasi_hipotesa_gejala` VALUES (11, 46);
INSERT INTO `relasi_hipotesa_gejala` VALUES (11, 47);
INSERT INTO `relasi_hipotesa_gejala` VALUES (11, 48);
INSERT INTO `relasi_hipotesa_gejala` VALUES (11, 50);
INSERT INTO `relasi_hipotesa_gejala` VALUES (15, 22);
INSERT INTO `relasi_hipotesa_gejala` VALUES (15, 25);
INSERT INTO `relasi_hipotesa_gejala` VALUES (15, 33);
INSERT INTO `relasi_hipotesa_gejala` VALUES (15, 34);
INSERT INTO `relasi_hipotesa_gejala` VALUES (15, 35);
INSERT INTO `relasi_hipotesa_gejala` VALUES (15, 36);
INSERT INTO `relasi_hipotesa_gejala` VALUES (15, 43);
INSERT INTO `relasi_hipotesa_gejala` VALUES (15, 44);
INSERT INTO `relasi_hipotesa_gejala` VALUES (15, 50);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 22);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 23);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 37);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 38);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 39);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 40);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 41);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 42);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 45);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 49);
INSERT INTO `relasi_hipotesa_gejala` VALUES (12, 51);
INSERT INTO `relasi_hipotesa_gejala` VALUES (1, 2);
INSERT INTO `relasi_hipotesa_gejala` VALUES (1, 6);
INSERT INTO `relasi_hipotesa_gejala` VALUES (17, 53);
INSERT INTO `relasi_hipotesa_gejala` VALUES (17, 54);
INSERT INTO `relasi_hipotesa_gejala` VALUES (17, 60);
INSERT INTO `relasi_hipotesa_gejala` VALUES (17, 65);
INSERT INTO `relasi_hipotesa_gejala` VALUES (17, 73);
INSERT INTO `relasi_hipotesa_gejala` VALUES (17, 77);
INSERT INTO `relasi_hipotesa_gejala` VALUES (17, 79);
INSERT INTO `relasi_hipotesa_gejala` VALUES (18, 54);
INSERT INTO `relasi_hipotesa_gejala` VALUES (18, 58);
INSERT INTO `relasi_hipotesa_gejala` VALUES (18, 59);
INSERT INTO `relasi_hipotesa_gejala` VALUES (18, 61);
INSERT INTO `relasi_hipotesa_gejala` VALUES (18, 66);
INSERT INTO `relasi_hipotesa_gejala` VALUES (18, 67);
INSERT INTO `relasi_hipotesa_gejala` VALUES (18, 68);
INSERT INTO `relasi_hipotesa_gejala` VALUES (18, 69);
INSERT INTO `relasi_hipotesa_gejala` VALUES (18, 77);
INSERT INTO `relasi_hipotesa_gejala` VALUES (19, 53);
INSERT INTO `relasi_hipotesa_gejala` VALUES (19, 62);
INSERT INTO `relasi_hipotesa_gejala` VALUES (19, 67);
INSERT INTO `relasi_hipotesa_gejala` VALUES (19, 68);
INSERT INTO `relasi_hipotesa_gejala` VALUES (19, 69);
INSERT INTO `relasi_hipotesa_gejala` VALUES (19, 71);
INSERT INTO `relasi_hipotesa_gejala` VALUES (19, 75);
INSERT INTO `relasi_hipotesa_gejala` VALUES (19, 79);
INSERT INTO `relasi_hipotesa_gejala` VALUES (19, 81);
INSERT INTO `relasi_hipotesa_gejala` VALUES (20, 53);
INSERT INTO `relasi_hipotesa_gejala` VALUES (20, 54);
INSERT INTO `relasi_hipotesa_gejala` VALUES (20, 59);
INSERT INTO `relasi_hipotesa_gejala` VALUES (20, 61);
INSERT INTO `relasi_hipotesa_gejala` VALUES (20, 63);
INSERT INTO `relasi_hipotesa_gejala` VALUES (20, 64);
INSERT INTO `relasi_hipotesa_gejala` VALUES (20, 65);
INSERT INTO `relasi_hipotesa_gejala` VALUES (20, 68);
INSERT INTO `relasi_hipotesa_gejala` VALUES (20, 70);
INSERT INTO `relasi_hipotesa_gejala` VALUES (21, 53);
INSERT INTO `relasi_hipotesa_gejala` VALUES (21, 54);
INSERT INTO `relasi_hipotesa_gejala` VALUES (21, 55);
INSERT INTO `relasi_hipotesa_gejala` VALUES (21, 67);
INSERT INTO `relasi_hipotesa_gejala` VALUES (22, 53);
INSERT INTO `relasi_hipotesa_gejala` VALUES (22, 59);
INSERT INTO `relasi_hipotesa_gejala` VALUES (22, 60);
INSERT INTO `relasi_hipotesa_gejala` VALUES (22, 66);
INSERT INTO `relasi_hipotesa_gejala` VALUES (22, 69);
INSERT INTO `relasi_hipotesa_gejala` VALUES (22, 79);
INSERT INTO `relasi_hipotesa_gejala` VALUES (22, 81);
INSERT INTO `relasi_hipotesa_gejala` VALUES (23, 53);
INSERT INTO `relasi_hipotesa_gejala` VALUES (23, 54);
INSERT INTO `relasi_hipotesa_gejala` VALUES (23, 55);
INSERT INTO `relasi_hipotesa_gejala` VALUES (23, 58);
INSERT INTO `relasi_hipotesa_gejala` VALUES (23, 59);
INSERT INTO `relasi_hipotesa_gejala` VALUES (23, 61);
INSERT INTO `relasi_hipotesa_gejala` VALUES (23, 67);
INSERT INTO `relasi_hipotesa_gejala` VALUES (23, 69);
INSERT INTO `relasi_hipotesa_gejala` VALUES (23, 77);
INSERT INTO `relasi_hipotesa_gejala` VALUES (24, 53);
INSERT INTO `relasi_hipotesa_gejala` VALUES (24, 54);
INSERT INTO `relasi_hipotesa_gejala` VALUES (24, 55);
INSERT INTO `relasi_hipotesa_gejala` VALUES (24, 57);
INSERT INTO `relasi_hipotesa_gejala` VALUES (24, 67);
INSERT INTO `relasi_hipotesa_gejala` VALUES (24, 69);
INSERT INTO `relasi_hipotesa_gejala` VALUES (25, 63);
INSERT INTO `relasi_hipotesa_gejala` VALUES (25, 64);
INSERT INTO `relasi_hipotesa_gejala` VALUES (25, 67);
INSERT INTO `relasi_hipotesa_gejala` VALUES (25, 76);
INSERT INTO `relasi_hipotesa_gejala` VALUES (25, 81);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 82);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 83);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 84);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 85);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 86);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 87);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 88);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 89);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 90);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 93);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 94);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 95);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 97);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 98);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 99);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 102);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 103);
INSERT INTO `relasi_hipotesa_gejala` VALUES (26, 104);
INSERT INTO `relasi_hipotesa_gejala` VALUES (27, 82);
INSERT INTO `relasi_hipotesa_gejala` VALUES (27, 85);
INSERT INTO `relasi_hipotesa_gejala` VALUES (27, 88);
INSERT INTO `relasi_hipotesa_gejala` VALUES (27, 89);
INSERT INTO `relasi_hipotesa_gejala` VALUES (27, 91);
INSERT INTO `relasi_hipotesa_gejala` VALUES (27, 96);
INSERT INTO `relasi_hipotesa_gejala` VALUES (27, 100);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 82);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 83);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 84);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 85);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 87);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 88);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 89);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 91);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 92);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 96);
INSERT INTO `relasi_hipotesa_gejala` VALUES (28, 101);
INSERT INTO `relasi_hipotesa_gejala` VALUES (29, 82);
INSERT INTO `relasi_hipotesa_gejala` VALUES (29, 83);
INSERT INTO `relasi_hipotesa_gejala` VALUES (29, 84);
INSERT INTO `relasi_hipotesa_gejala` VALUES (29, 85);
INSERT INTO `relasi_hipotesa_gejala` VALUES (29, 88);
INSERT INTO `relasi_hipotesa_gejala` VALUES (29, 89);
INSERT INTO `relasi_hipotesa_gejala` VALUES (29, 91);
INSERT INTO `relasi_hipotesa_gejala` VALUES (29, 92);
INSERT INTO `relasi_hipotesa_gejala` VALUES (30, 82);
INSERT INTO `relasi_hipotesa_gejala` VALUES (30, 83);
INSERT INTO `relasi_hipotesa_gejala` VALUES (30, 84);
INSERT INTO `relasi_hipotesa_gejala` VALUES (30, 85);
INSERT INTO `relasi_hipotesa_gejala` VALUES (30, 86);
INSERT INTO `relasi_hipotesa_gejala` VALUES (30, 87);
INSERT INTO `relasi_hipotesa_gejala` VALUES (30, 88);
INSERT INTO `relasi_hipotesa_gejala` VALUES (30, 90);
INSERT INTO `relasi_hipotesa_gejala` VALUES (30, 93);
INSERT INTO `relasi_hipotesa_gejala` VALUES (30, 94);
INSERT INTO `relasi_hipotesa_gejala` VALUES (31, 82);
INSERT INTO `relasi_hipotesa_gejala` VALUES (31, 86);
INSERT INTO `relasi_hipotesa_gejala` VALUES (31, 87);
INSERT INTO `relasi_hipotesa_gejala` VALUES (31, 90);
INSERT INTO `relasi_hipotesa_gejala` VALUES (32, 82);
INSERT INTO `relasi_hipotesa_gejala` VALUES (32, 83);
INSERT INTO `relasi_hipotesa_gejala` VALUES (32, 84);
INSERT INTO `relasi_hipotesa_gejala` VALUES (32, 86);
INSERT INTO `relasi_hipotesa_gejala` VALUES (33, 82);
INSERT INTO `relasi_hipotesa_gejala` VALUES (33, 83);
INSERT INTO `relasi_hipotesa_gejala` VALUES (33, 84);
INSERT INTO `relasi_hipotesa_gejala` VALUES (33, 86);
INSERT INTO `relasi_hipotesa_gejala` VALUES (33, 95);

-- ----------------------------
-- Table structure for topik
-- ----------------------------
DROP TABLE IF EXISTS `topik`;
CREATE TABLE `topik`  (
  `id_topik` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_topik` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_topik`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of topik
-- ----------------------------
INSERT INTO `topik` VALUES (1, 'DIAGNOSA KERUSAKAN SEPEDA MOTOR');
INSERT INTO `topik` VALUES (3, 'ANEMIA, BATUK, DEMAM, FLU');
INSERT INTO `topik` VALUES (4, 'PENYAKIT UMUM');
INSERT INTO `topik` VALUES (6, 'Mendiagnosa Penyakit  Dari Akibat Bakteri Salmonella ');

-- ----------------------------
-- Table structure for topik_copy1
-- ----------------------------
DROP TABLE IF EXISTS `topik_copy1`;
CREATE TABLE `topik_copy1`  (
  `id_topik` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_topik` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_topik`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of topik_copy1
-- ----------------------------
INSERT INTO `topik_copy1` VALUES (1, 'DIAGNOSA KERUSAKAN SEPEDA MOTOR');
INSERT INTO `topik_copy1` VALUES (3, 'ANEMIA, BATUK, DEMAM, FLU');
INSERT INTO `topik_copy1` VALUES (4, 'PENYAKIT UMUM');
INSERT INTO `topik_copy1` VALUES (5, 'DIAGNOSA PENYAKIT GINJAL');

SET FOREIGN_KEY_CHECKS = 1;
