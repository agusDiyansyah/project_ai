<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Gejala extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS gejala;
		");
		$this->db->query("
			CREATE TABLE `gejala` (
				`id_gejala` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`id_topik` int(11),
				`nama_gejala` varchar(255),
				`bobot` decimal(10,2),
				`photo` varchar(255),
				`keterangan` text,
				PRIMARY KEY (`id_gejala`)
			);
		");
	}

	public function down () {}
	
}